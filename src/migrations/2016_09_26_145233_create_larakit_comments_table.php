<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLarakitCommentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('larakit__comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commentable_id');
            $table->string('commentable_type');
            $table->text('comment');
            $table->integer('author_id');
            $table->integer('reply_id');
            $table->timestamps();
            $table->index(['commentable_id', 'commentable_type'], 'obj_idx');
            $table->index(['author_id'], 'author_idx');
            $table->index(['reply_id'], 'reply_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('larakit__comments');
    }
}
