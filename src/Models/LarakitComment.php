<?php
/**
 * Created by PhpStorm.
 * User: koksharov
 * Date: 30.11.16
 * Time: 14:08
 */

namespace Larakit\Models;

use App\Me\Me;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Larakit\Helpers\HelperDate;

/**
 * Class LarakitComment
 *
 * @package Larakit\Models
 * @mixin \Eloquent
 */
class LarakitComment extends Model {
    
    protected $table    = 'larakit__comments';
    protected $appends  = [
        'dt',
        'is_my',
    ];
    protected $hidden   = [
        'commentable_id',
        'commentable_type',
        'created_at',
        'updated_at',
    ];
    protected $fillable = [
        'commentable_id',
        'commentable_type',
        'comment',
        'author_id',
        'reply_id',
    ];
    
    static protected $object_types = [];
    
    static function registerObjectType($model_class, $key = null) {
        self::$object_types[$model_class] = ($key ? $key : md5($model_class));
    }
    
    function author() {
        return $this->belongsTo(User::class, 'author_id');
    }
    
    function reply() {
        return $this->belongsTo(LarakitComment::class, 'reply_id');
    }
    
    function __toString() {
        return 'Комментарий от ' . $this->author . ': ' . $this->comment;
    }
    
    static function getObjectType($key) {
        return array_search($key, self::$object_types);
    }
    
    static function getObjectCode($model_name) {
        return Arr::get(self::$object_types, $model_name);
    }
    
    function getDtAttribute() {
        return HelperDate::fromDatetime($this->created_at);
    }
    
    function getCommentAttribute($value) {
        $config = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($config);
        $value = $purifier->purify($value);
//        $value = strip_tags($value);
        return nl2br($value);
    }
    
    function getIsMyAttribute() {
        return $this->author_id == Me::id();
    }
    
    public function commentable() {
        return $this->morphTo();
    }
    
    public function votes() {
        return $this->morphMany(LarakitVote::class, 'voteable');
    }
    
    static function addToObject(Model $o, $comment, $reply_id = null) {
        $usr_id = 0;
        if(\Auth::getUser()) {
            $usr_id = \Auth::getUser()->id;
        }
        $comment = LarakitComment::create([
            'comment'          => $comment,
            'author_id'        => $usr_id,
            'reply_id'         => (int) $reply_id,
            'commentable_id'   => $o->id,
            'commentable_type' => $o->getMorphClass(),
        ]);
        
        return $comment;
    }
    
}

LarakitComment::creating(function ($model) {
    if(!$model->commentable_type) {
        $model->commentable_type = '';
    }
    if(!$model->commentable_id) {
        $model->commentable_id = 0;
    }
    if(!$model->author_id) {
        $model->author_id = Me::id();
    }
    if(!$model->reply_id) {
        $model->reply_id = 0;
    }
});