<?php
\Larakit\Boot::register_provider(\Larakit\Comments\LarakitServiceProvider::class);

\Larakit\Twig::register_function('larakit_comments', function ($model) {
    $model_name = get_class($model);
    $code       = \Larakit\Models\LarakitComment::getObjectCode($model_name);
    $vote_type  = \Larakit\Models\LarakitVote::getObjectCode(\Larakit\Models\LarakitComment::class);
    if(!$code) {
        return 'Запрещено комментировать объекты типа ' . $model_name;
    }
    
    return '
<comments
:comment_model="\'' . $code . '\'"
:vote_type="\'' . $vote_type . '\'"
:comment_model_id="' . $model->id . '"
:comment_token="\'' . csrf_token() . '\'">
</comments>';
});

\Larakit\Widgets\ManagerWidget::register(\Larakit\Comments\WidgetLarakitComments::class, __DIR__ . '/views');