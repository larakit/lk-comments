<?php
namespace Larakit\Comments;

use Larakit\Widgets\Widget;

class WidgetLarakitComments extends Widget {
    
    protected $object;
    
    function setModel($object) {
        return $this->set('model', $object);
    }
}