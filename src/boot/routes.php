<?php
use Larakit\Models\LarakitComment;

\Larakit\Route\Route::ajax('comments')
    ->addMiddleware(['web','auth'])
    ->setNamespace('Larakit\Comments')
    ->addSegment('{object_type}')
    ->addSegment('{object_id}')
    //                    ->addPattern('object_type', '[a-z0-9]+')
    ->setUses(function ($object_type, $object_id) {
        $model_name = LarakitComment::getObjectType($object_type);
        if(!$model_name) {
            throw new Exception('Не известный науке тип!');
        }
        if(!class_exists($model_name)) {
            throw new Exception('Не известный науке тип!');
        }
        $models = LarakitComment::orderBy('id', 'asc')
            ->with([
                'author',
                'votes',
                'reply',
                'reply.author',
            ])
            ->where('commentable_id', $object_id)
            ->where('commentable_type', $model_name)
            ->get();
        return [
            'result' => 'success',
            'models' => $models,
        ];
    })
    ->put('get')
    ->setUses(function ($object_type, $object_id) {
        $id = \Request::get('id');
        if(!$id) {
            throw new Exception('Удаление запрещено!');
        }
        $comment = LarakitComment::find($id);
        if(!$comment) {
            throw new Exception('Удаление запрещено!');
        }
        $usr_id = 0;
        if(\Auth::getUser()) {
            $usr_id = \Auth::getUser()->id;
        }
        if($comment->author_id != $usr_id) {
            throw new Exception('Удаление запрещено!');
        }
        $comment->delete();
        
        return [
            'result'  => 'success',
            'message' => 'Ваш комментарий успешно удален',
        ];
        
    })
    ->put('delete')
    ->setUses(function ($object_type, $object_id) {
        $usr_id = 0;
        if(\Auth::getUser()) {
            $usr_id = \Auth::getUser()->id;
        }
        $model_name = LarakitComment::getObjectType($object_type);
        if(!$model_name) {
            throw new Exception('Не известный науке тип!');
        }
        if(!class_exists($model_name)) {
            throw new Exception('Не известный науке тип!');
        }
        $o = $model_name::find($object_id);
        if(!$o) {
            throw new Exception('Комментируемый объект отсутствует!');
        }
        
        $comment = LarakitComment::create([
            'comment'          => Request::get('comment'),
            'reply_id'         => (int) Request::get('reply_id'),
            'author_id'        => $usr_id,
            'commentable_id'   => $o->id,
            'commentable_type' => $o->getMorphClass(),
        ]);
        \Larakit\TelegramBot::add('НОВЫЙ КОММЕНТАРИЙ для "' . $o . '"');
        \Larakit\TelegramBot::add(\App\Me\Me::hashtag() . ' прокомментировал:');
        \Larakit\TelegramBot::add($comment->comment);
        if($comment->reply_id) {
            \Larakit\TelegramBot::add('ОТВЕТ НА: ' . $comment->reply);
        }
        \Larakit\TelegramBot::send('notify');
        
        return [
            'result'  => 'success',
            'anchor'  => '#c' . $comment->id,
            'message' => 'Ваш комментарий успешно добавлен',
        ];
        
    })
    ->put('post');
